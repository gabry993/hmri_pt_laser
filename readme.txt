joystick.py
Simple node that takes the input from the joystick (i.e. from the joy topic) and publish a twist message on the laser_dot/velocity topic.
I'm making such semplification: the x axis of the joystick will determine the speed of the laser dot along the x axis, the y axis of the joystick will do the same for the y axis.

laser_dot.py
The node that represents the laser dot on the plane, with its x and y position, which are published on its topic. It subscribes a topic to get the velocity of the point. It then simply updates the position at a fixed frequence proportionally to the speedalong each axis.
NOTE: this class is used only when working with the joystick, but could work also in the same way for the trajectory drawing.

pt_laser.py and wall_pt_laser.py
They are implementing the kinematic and the communication with the turret. For now they implement also the logic to draw shapes (8 and square), but that could be easily moved out, in such a way that the movement will happen as for the joystick, following the position published by laser_dot.
The kinematics and the implementation are discussed in the report 
