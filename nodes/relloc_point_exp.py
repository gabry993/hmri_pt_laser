#!/usr/bin/env python

import collections
import copy
import threading
import csv
from datetime import datetime
from random import shuffle

import numpy as np
from sound_play.libsoundplay import SoundClient
import rosbag

import rospy
import rostopic

import actionlib
from std_msgs.msg import Bool, Empty, String, Float32, Header
import tf2_ros
import tf2_geometry_msgs
import tf_conversions as tfc
import PyKDL as kdl

from geometry_msgs.msg import Pose, PoseStamped, TransformStamped, PointStamped, Point

from hmri_msgs.msg import MotionRellocAction, MotionRellocFeedback, MotionRellocResult
from hmri_msgs.msg import GoToAction, GoToGoal
from hmri_msgs.msg import FollowShapeAction, FollowShapeGoal, MotionRellocGoal

import relloclib

csv_result_header = ["user_id", "shape", "position_id", "user_x", "user_y", "error_x", "error_y", "physical_error_x", "physical_error_y"]

class PointingExpNode:
    def __init__(self):
        self.target_position_list = self.generate_position_from_file()
        self.physical_error_list = []
        self.error_list = []
        self.state_pub = rospy.Publisher("state", String, queue_size = 1)
        self.target_pub = rospy.Publisher('target_point', PointStamped, queue_size = 10)
        self.relloc_human_pos = PointStamped()
        self.relloc_human_pos_sub = rospy.Subscriber('relloc_human_pos', PointStamped, self.relloc_human_cb)
        self.end_of_exp = False
        self.user_id = rospy.get_param('~user_id', 1)
        self.shape = rospy.get_param('~shape', "8")
        self.state_pub.publish("following_target")
        self.global_state_pub = rospy.Publisher("global_state", String, queue_size=10)
        self.target_position = rospy.Publisher('target_position', PointStamped, queue_size = 10)
        self.timewindow = rospy.get_param('~timewindow', 5.0)
        self.sample_size = rospy.get_param('~sample_size', 250)
        self.sound_client = SoundClient()
        self.button_sub = rospy.Subscriber("/idsia/myo2/button", Bool, self.button_cb)
        self.relloc_client = actionlib.SimpleActionClient('relloc_action', MotionRellocAction)
        self.relloc_client.wait_for_server()

    def button_cb(self, msg):
            self.button = msg.data

    def relloc_human_cb(self, msg):
        self.relloc_human_pos = msg

    def generate_position_from_file(self):
        pos_point_list = []
        with open('/home/gabry/catkin_ws/src/hmri_pt_laser/experiment_pos/3target_pos.csv', 'rb') as f:
            reader = csv.DictReader(f, delimiter=',')
            aux = list(reader)
            print aux
        for pos in aux:
            point = Point()
            point.x = float(pos['x'])
            point.y = float(pos['y'])
            point.z = 0.0
            pos_point_list.append((pos['id'], point))
        #shuffle(pos_point_list)
        print(pos_point_list)
        return pos_point_list

    def run(self):
        user_in = raw_input("Target Calibration Needed? (y/n)\n")
        if user_in == 'y':
            calibrated = False
            while not calibrated:
                self.state_pub.publish("following_target")
                for position in self.target_position_list:
                    msg_point = PointStamped()
                    msg_point.header = Header()
                    msg_point.header.stamp = rospy.Time.now()
                    msg_point.point = position[1]
                    self.target_pub.publish(msg_point)
                    rospy.sleep(5)
                msg_point = PointStamped()
                msg_point.header = Header()
                msg_point.header.stamp = rospy.Time.now()
                msg_point.point.x = 2.0
                msg_point.point.y = 2.0
                self.target_pub.publish(msg_point)
                rospy.sleep(5)
                user_in = raw_input("Type n to Calibrate Again\n")
                if user_in != 'n':
                    calibrated = True

        while not rospy.is_shutdown() and not self.end_of_exp:
            try:
                self.global_state_pub.publish("EXPERIMENT_START")
                csv_lines = [csv_result_header]
                print("Press Button To Start\n")
                while not self.button:
                    pass
                rospy.sleep(2)
                for i in range(0, 3):
                    self.global_state_pub.publish("RELLOC_START")
                    goal = MotionRellocGoal()
                    goal.max_duration = rospy.rostime.Duration(5) #(secs=5, nsecs=0)
                    goal.min_samples = 250
                    self.relloc_client.send_goal_and_wait(goal)
                    self.global_state_pub.publish("RELLOC_END")
                    for position in self.target_position_list:
                        self.global_state_pub.publish("START_SESSION")
                        msg_point = PointStamped()
                        msg_point.header = Header()
                        msg_point.header.stamp = rospy.Time.now()
                        msg_point.point = position[1]
                        self.target_position.publish(msg_point)
                        print("Get Ready For Next Target!")
                        self.global_state_pub.publish("COUNTING_10")
                        self.sound_client.playWave('/home/gabry/catkin_ws/src/hmri_pt_laser/nodes/sounds/countdown.wav')
                        rospy.sleep(10)
                        self.global_state_pub.publish("START_NO_FDB")
                        print("Point Target Without Feedback\n")
                        self.global_state_pub.publish("MOVING_NO_FDB")
                        rospy.sleep(5)
                        #self.sound_client.playWave('/home/gabry/catkin_ws/src/hmri_pt_laser/nodes/sounds/beep.wav')
                        self.global_state_pub.publish("POINTING_NO_FDB")
                        print("Stay There 5s\n")
                        rospy.sleep(5)
                        self.sound_client.playWave('/home/gabry/catkin_ws/src/hmri_pt_laser/nodes/sounds/beep.wav')
                        self.global_state_pub.publish("END_NO_FDB")
                        print("Rest\n")
                        self.global_state_pub.publish("RESTING")
                        print("Press Button To Go On\n")
                        while not self.button:
                            pass
                        self.sound_client.playWave('/home/gabry/catkin_ws/src/hmri_pt_laser/nodes/sounds/countdown.wav')
                        rospy.sleep(10)
                        self.global_state_pub.publish("START_FDB")
                        self.state_pub.publish("following_arm")
                        print("Point Target With Feedback")
                        self.global_state_pub.publish("MOVING_FDB")
                        rospy.sleep(5)
                        #self.sound_client.playWave('/home/gabry/catkin_ws/src/hmri_pt_laser/nodes/sounds/beep.wav')
                        self.global_state_pub.publish("POINTING_FDB")
                        print("Stay There 5s\n")
                        rospy.sleep(5)
                        self.sound_client.playWave('/home/gabry/catkin_ws/src/hmri_pt_laser/nodes/sounds/beep.wav')
                        self.global_state_pub.publish("END_FDB")
                        self.state_pub.publish("following_target")
                        self.global_state_pub.publish("END_SESSION")
                        print("Press Button To Go On\n")
                        while not self.button:
                            pass
                self.end_of_exp = True
            except rospy.ROSException, e:
                if e.message == 'ROS time moved backwards':
                    rospy.logwarn("Saw a negative time change. Resetting internal state...")
                    self.reset_state()
        print("Experiment End")
        self.global_state_pub.publish("EXPERIMENT_END")

if __name__ == '__main__':
    rospy.init_node('pointing_exp_node')

    experiment = PointingExpNode()
    try:
        experiment.run()
    except rospy.ROSInterruptException:
        rospy.logdebug('Exiting')
        pass
