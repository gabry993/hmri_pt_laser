#!/usr/bin/env python

import collections
import copy
import threading
import csv
from datetime import datetime
from random import shuffle

import numpy as np
from sound_play.libsoundplay import SoundClient
import rosbag

import rospy
import rostopic

import actionlib
from std_msgs.msg import Bool, Empty, String, Float32, Header
import tf2_ros
import tf2_geometry_msgs
import tf_conversions as tfc
import PyKDL as kdl

from geometry_msgs.msg import Pose, PoseStamped, TransformStamped, PointStamped, Point

from hmri_msgs.msg import MotionRellocAction, MotionRellocFeedback, MotionRellocResult
from hmri_msgs.msg import GoToAction, GoToGoal
from hmri_msgs.msg import FollowShapeAction, FollowShapeGoal, MotionRellocGoal

import relloclib

csv_result_header = ["user_id", "shape", "position_id", "user_x", "user_y", "error_x", "error_y", "physical_error_x", "physical_error_y"]

class ExperimentNode:
    def __init__(self):
        self.position_list = self.generate_position_from_file()
        self.physical_error_list = []
        self.error_list = []
        self.relloc_client = actionlib.SimpleActionClient('relloc_action', MotionRellocAction)
        self.relloc_client.wait_for_server()
        self.state_pub = rospy.Publisher("state", String, queue_size = 1)
        self.target_pub = rospy.Publisher('target_point', PointStamped, queue_size = 10)
        self.relloc_human_pos = PointStamped()
        self.relloc_human_pos_sub = rospy.Subscriber('relloc_human_pos', PointStamped, self.relloc_human_cb)
        self.end_of_exp = False
        self.user_id = rospy.get_param('~user_id', 1)
        self.shape = rospy.get_param('~shape', "8")
        self.state_pub.publish("following_target")
        self.global_state_pub = rospy.Publisher("global_state", String, queue_size=10)
        self.gt_position = rospy.Publisher('gt_aux', PointStamped, queue_size = 10)
        self.timewindow = rospy.get_param('~timewindow', 5.0)
        self.sample_size = rospy.get_param('~sample_size', 150)

    def relloc_human_cb(self, msg):
        self.relloc_human_pos = msg

    def generate_position_from_file(self):
        pos_point_list = []
        with open('/home/gabry/catkin_ws/src/hmri_pt_laser/experiment_pos/pos.csv', 'rb') as f:
            
            reader = csv.DictReader(f, delimiter=',')
            aux = list(reader)
            print aux
        for pos in aux:
            point = Point()
            point.x = float(pos['x'])
            point.y = float(pos['y'])
            point.z = 0.0
            pos_point_list.append((pos['id'], point))
        shuffle(pos_point_list)
        print(pos_point_list)
        return pos_point_list

    def run(self):
        while not rospy.is_shutdown() and not self.end_of_exp:
            try:
                self.global_state_pub.publish("EXPERIMENT_START")
                csv_lines = [csv_result_header]
                for position in self.position_list:
                    user_in = raw_input("Type to get next position\n")
                    self.state_pub.publish("following_target")
                    self.global_state_pub.publish("MOVING_TO_TARGET_POSITION")
                    msg_point = PointStamped()
                    msg_point.header = Header()
                    msg_point.header.stamp = rospy.Time.now()
                    msg_point.point = position[1]
                    self.target_pub.publish(msg_point)
                    self.gt_position.publish(msg_point)
                    print("Next position is:")
                    print(position)
                    user_in = raw_input("Type when ready to go\n")
                    while user_in == "n":
                        self.state_pub.publish("following_target")
                        msg_point = PointStamped()
                        msg_point.header = Header()
                        msg_point.header.stamp = rospy.Time.now()
                        msg_point.point = position[1]
                        self.target_pub.publish(msg_point)
                        self.gt_position.publish(msg_point)
                        user_in = raw_input("Type when ready to go\n")
                    goal = MotionRellocGoal()
                    goal.max_duration = rospy.rostime.Duration(self.timewindow) #(secs=5, nsecs=0)
                    goal.min_samples = self.sample_size
                    self.relloc_client.send_goal(goal)
                    user_in = raw_input("Type to go to next\n")
                    print(self.relloc_human_pos.point)
                self.end_of_exp = True
            except rospy.ROSException, e:
                if e.message == 'ROS time moved backwards':
                    rospy.logwarn("Saw a negative time change. Resetting internal state...")
                    self.reset_state()
        print("Experiment End")
        self.global_state_pub.publish("EXPERIMENT_END")
        result_file = open('/home/gabry/catkin_ws/src/hmri_pt_laser/experiment_res/res_'+str(datetime.now())+'.csv', 'w')
        with result_file:
            writer = csv.writer(result_file)
            writer.writerows(csv_lines)
        print("Writing csv complete")


if __name__ == '__main__':
    rospy.init_node('experiment')

    experiment = ExperimentNode()

    try:
        experiment.run()
    except rospy.ROSInterruptException:
        rospy.logdebug('Exiting')
        pass
