#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import tf
import tf_conversions as tfc
from geometry_msgs.msg import PointStamped, TransformStamped, Pose, PoseStamped, Point
from std_msgs.msg import Bool, String
from visualization_msgs.msg import Marker
import math
import numpy as np
import PyKDL as kdl
#28mm from plate to axis
dz_ground = 0#0.415#0.725 # 72.5cm
dz_top_building =0#0.059
dz_to_motor_1_flange=0#0# self.dz_ground + 0.0995# 4cm
dz_to_motor_2_flange=0#0.0543 # 5.43cm

class tfTurret():

    def __init__(self):
        # Initialize new node
        rospy.init_node('tf_turret', anonymous=True)
        self.turret_height = rospy.get_param("~turret_height", 0.415) #default is kobuki
        self.dz_ground = self.turret_height#0.725 # 72.5cm
        self.dz_top_building = 0.059
        self.dz_to_motor_1_flange =self.dz_ground + 0.0995# 4cm
        self.dz_to_motor_2_flange = 0.0543 # 5.43cm
        self.rate = rospy.Rate(50) # 50hz
        self.target_sub = rospy.Subscriber('target_point', PointStamped, self.update_target_point)
        self.joint_state_pub = rospy.Publisher('joint_state', JointState, queue_size=10)
        self.br = tf.TransformBroadcaster()
        self.listener = tf.TransformListener()
        self.target_point = PointStamped()
        self.last_target_point = PointStamped()
        self.pan_angle = 0.0
        self.tilt_angle = 0.0
        self.tf = tf.TransformListener()
        self.marker_pub = rospy.Publisher('laser_marker', Marker, queue_size=10)
        self.marker_id = 0
        self.state = "following_target"
        self.pose_target_sub = rospy.Subscriber('idsia/arm_pointer', PoseStamped, self.update_target_pose_point)
        self.state_sub = rospy.Subscriber("state", String, self.set_state_cb)
        self.relloc_human_pos_pub = rospy.Publisher('relloc_human_pos', PointStamped, queue_size=10)
        self.gt_position_pub = rospy.Publisher('gt_point', PointStamped, queue_size = 10)
        self.gt_aux_sub = rospy.Subscriber('gt_aux', PointStamped, self.update_gt_cb)
        self.gt_position = PointStamped()
        self.kobuki_goal_sub = rospy.Subscriber('/mobile_base/commands/goal_point', PointStamped, self.kobuki_goal_cb)
        self.kobuki_goal = PointStamped()


    def kobuki_goal_cb(self, msg):
        self.kobuki_goal = msg

    def update_gt_cb(self, msg):
        self.gt_position = msg

    def set_state_cb(self, msg):
        self.state = msg.data
        print(self.state)
    def publish_tf_tree(self, m1, m2):
        self.br.sendTransform((0, 0, self.dz_to_motor_1_flange),
                         tf.transformations.quaternion_from_euler(0, 0, m1),
                         rospy.Time.now(),
                         "pan_link",
                         "base_link")
        self.br.sendTransform((0, 0, self.dz_to_motor_2_flange),
                         tf.transformations.quaternion_from_euler(np.pi/2, -np.pi/2, 0),
                         rospy.Time.now(),
                         "tilt_link",
                         "pan_link")
        self.br.sendTransform((0, 0, 0),
                         tf.transformations.quaternion_from_euler(0, 0, m2 - np.pi/2),
                         rospy.Time.now(),
                         "laser_pointer_link",
                         "tilt_link")
        self.br.sendTransform((0.075, 0, 0),
                         tf.transformations.quaternion_from_euler(0, 0, -np.pi/2),
                         rospy.Time.now(),
                         "laser_link",
                         "laser_pointer_link")
        self.br.sendTransform((5.0, 0, 0),
                         tf.transformations.quaternion_from_euler(0, 0, 0),
                         rospy.Time.now(),
                         "debug_link",
                         "laser_link")
        self.rate.sleep()



    def run(self):
        while not rospy.is_shutdown():
            self.publish_tf_tree(self.pan_angle, self.tilt_angle)
            self.gt_position_pub.publish(self.gt_position)

    def update_target_pose_point(self, msg):
        if self.state == "following_arm":
            self.stamped_point = PointStamped()
            self.stamped_point.header = msg.header
            self.stamped_point.header.stamp = rospy.Time() #= msg.header
            self.stamped_point.point = msg.pose.position
            print(self.stamped_point)
            self.target_point = self.tf.transformPoint("base_link", self.stamped_point)
            #print(self.target_point)
            #self.target_point = msg.pose.position
            self.pan_angle, self.tilt_angle = self.ik(self.target_point.point.x, self.target_point.point.y, self.target_point.point.z)
            self.br.sendTransform((self.target_point.point.x, self.target_point.point.y, self.target_point.point.z),
                             tf.transformations.quaternion_from_euler(0, 0, 0),
                             rospy.Time.now(),
                             "debug_point",
                             "base_link")
            laser_marker = Marker()
            laser_marker.header.frame_id = "/base_link"
            laser_marker.header.stamp    = rospy.get_rostime()
            laser_marker.ns = "robot"
            laser_marker.id = self.marker_id
            self.marker_id +=1
            pose = Pose()
            pose.position = self.target_point
            laser_marker.pose.position = self.target_point.point
            laser_marker.scale.x = 0.01
            laser_marker.scale.y = 0.01
            laser_marker.scale.z = 0.01

            laser_marker.color.r = 0.0
            laser_marker.color.g = 1.0
            laser_marker.color.b = 0.0
            laser_marker.color.a = 1.0

            laser_marker.lifetime = rospy.Duration(10)
            self.publish_joint_state(self.pan_angle, self.tilt_angle)
            self.marker_pub.publish(laser_marker)
        elif self.state == "marking_kobuki_goal":
            self.stamped_point = self.kobuki_goal
            self.stamped_point.header.stamp = rospy.Time()
            self.target_point = self.tf.transformPoint("base_link", self.stamped_point)
            #print(self.target_point)
            #self.target_point = msg.pose.position
            self.pan_angle, self.tilt_angle = self.ik(self.target_point.point.x, self.target_point.point.y, self.target_point.point.z)
            self.br.sendTransform((self.target_point.point.x, self.target_point.point.y, self.target_point.point.z),
                             tf.transformations.quaternion_from_euler(0, 0, 0),
                             rospy.Time.now(),
                             "debug_point",
                             "base_link")
            laser_marker = Marker()
            laser_marker.header.frame_id = "/base_link"
            laser_marker.header.stamp    = rospy.get_rostime()
            laser_marker.ns = "robot"
            laser_marker.id = self.marker_id
            self.marker_id +=1
            pose = Pose()
            pose.position = self.target_point
            laser_marker.pose.position = self.target_point.point
            laser_marker.scale.x = 0.01
            laser_marker.scale.y = 0.01
            laser_marker.scale.z = 0.01

            laser_marker.color.r = 0.0
            laser_marker.color.g = 1.0
            laser_marker.color.b = 0.0
            laser_marker.color.a = 1.0

            laser_marker.lifetime = rospy.Duration(10)
            self.publish_joint_state(self.pan_angle, self.tilt_angle)
            self.marker_pub.publish(laser_marker)
        elif self.state == "following_kobuki_trajectory":
            #self.stamped_point = self.kobuki_goal
            #self.stamped_point.header.stamp = rospy.Time()
            self.target_point = PointStamped()
            self.target_point.point.x = 0.2
            self.target_point.point.y = 0.0
            self.target_point.point.z = 0.0
            #self.tf.transformPoint("base_link", self.stamped_point)
            #print(self.target_point)
            #self.target_point = msg.pose.position
            self.pan_angle, self.tilt_angle = self.ik(self.target_point.point.x, self.target_point.point.y, self.target_point.point.z)
            self.br.sendTransform((self.target_point.point.x, self.target_point.point.y, self.target_point.point.z),
                             tf.transformations.quaternion_from_euler(0, 0, 0),
                             rospy.Time.now(),
                             "debug_point",
                             "base_link")
            laser_marker = Marker()
            laser_marker.header.frame_id = "/base_link"
            laser_marker.header.stamp    = rospy.get_rostime()
            laser_marker.ns = "robot"
            laser_marker.id = self.marker_id
            self.marker_id +=1
            pose = Pose()
            pose.position = self.target_point
            laser_marker.pose.position = self.target_point.point
            laser_marker.scale.x = 0.01
            laser_marker.scale.y = 0.01
            laser_marker.scale.z = 0.01

            laser_marker.color.r = 0.0
            laser_marker.color.g = 1.0
            laser_marker.color.b = 0.0
            laser_marker.color.a = 1.0

            laser_marker.lifetime = rospy.Duration(10)
            self.publish_joint_state(self.pan_angle, self.tilt_angle)
            self.marker_pub.publish(laser_marker)
        elif self.state == "marking_relloc":
            try:
                (trans,rot) = self.listener.lookupTransform('base_link', 'human_footprint', rospy.Time(0))
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                pass
            self.target_point.point.x = trans[0]
            self.target_point.point.y = trans[1]
            self.target_point.point.z = trans[2]
            self.relloc_human_pos_pub.publish(self.target_point)
            self.pan_angle, self.tilt_angle = self.ik(self.target_point.point.x, self.target_point.point.y, self.target_point.point.z)
            self.br.sendTransform((self.target_point.point.x, self.target_point.point.y, self.target_point.point.z),
                             tf.transformations.quaternion_from_euler(0, 0, 0),
                             rospy.Time.now(),
                             "debug_point",
                             "base_link")
            laser_marker = Marker()
            laser_marker.header.frame_id = "/base_link"
            laser_marker.header.stamp    = rospy.get_rostime()
            laser_marker.ns = "robot"
            laser_marker.id = self.marker_id
            self.marker_id +=1
            pose = Pose()
            pose.position = self.target_point
            laser_marker.pose.position = self.target_point.point
            laser_marker.scale.x = 0.01
            laser_marker.scale.y = 0.01
            laser_marker.scale.z = 0.01

            laser_marker.color.r = 0.0
            laser_marker.color.g = 1.0
            laser_marker.color.b = 0.0
            laser_marker.color.a = 1.0

            laser_marker.lifetime = rospy.Duration(10)
            self.publish_joint_state(self.pan_angle, self.tilt_angle)
            self.marker_pub.publish(laser_marker)

    def update_target_point(self, msg):
        if self.state =="following_target":
            self.target_point = msg
            self.pan_angle, self.tilt_angle = self.ik(msg.point.x, msg.point.y, msg.point.z)
            self.br.sendTransform((msg.point.x, msg.point.y, msg.point.z),
                             tf.transformations.quaternion_from_euler(0, 0, 0),
                             rospy.Time.now(),
                             "debug_point",
                             "base_link")
            laser_marker = Marker()
            laser_marker.header.frame_id = "/base_link"
            laser_marker.header.stamp    = rospy.get_rostime()
            laser_marker.ns = "robot"
            laser_marker.id = self.marker_id
            self.marker_id +=1
            pose = Pose()
            pose.position = msg.point
            laser_marker.pose.position = msg.point
            laser_marker.scale.x = 0.01
            laser_marker.scale.y = 0.01
            laser_marker.scale.z = 0.01

            laser_marker.color.r = 0.0
            laser_marker.color.g = 1.0
            laser_marker.color.b = 0.0
            laser_marker.color.a = 1.0

            laser_marker.lifetime = rospy.Duration(10)
            self.publish_joint_state(self.pan_angle, self.tilt_angle)
            self.marker_pub.publish(laser_marker)
        

    def publish_joint_state(self, pan, tilt):
        joint_state_msg = JointState()
        joint_state_msg.header = Header()
        joint_state_msg.header.stamp = rospy.Time.now()
        joint_state_msg.name = ['pan_link', 'laser_link']
        joint_state_msg.position = [math.degrees(pan), math.degrees(tilt - np.pi/2)]#+1.402397559246]
        joint_state_msg.velocity = []
        joint_state_msg.effort = []
        self.joint_state_pub.publish(joint_state_msg)

    def ik(self, x, y, z):
        pan = np.math.atan2(y, x)
        dz = self.dz_to_motor_1_flange + self.dz_to_motor_2_flange - z
        b = np.math.sqrt(x**2 + y**2)
        c = np.math.sqrt(dz**2 + b**2)
        tilt1 = np.math.atan2(b, dz)
        tilt2= np.math.atan2(c, 0.075)
        tilt = -np.pi/2+ tilt1 + tilt2
        return pan, tilt

        # (m1_pos, m2_pos) = self.turret_ik(**msg.position)


if __name__ == '__main__':
    try:
        turret = tfTurret()
        turret.run()
    except rospy.ROSInterruptException:
        pass