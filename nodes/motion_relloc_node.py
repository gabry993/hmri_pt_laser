#!/usr/bin/env python

import collections
import copy
import threading

import numpy as np
from sound_play.libsoundplay import SoundClient

import rospy
import rostopic

import actionlib
from std_msgs.msg import Bool, String
import tf2_ros
import tf2_geometry_msgs
import tf_conversions as tfc
import PyKDL as kdl

from geometry_msgs.msg import Pose, PoseStamped, TransformStamped, PointStamped

from hmri_msgs.msg import MotionRellocAction, MotionRellocFeedback, MotionRellocResult
from hmri_msgs.msg import GoToAction, GoToGoal
from hmri_msgs.msg import FollowShapeAction, FollowShapeGoal

import relloclib

class MotionRellocNode:
    def __init__(self):
        #action_ns = rospy.get_param('~action_ns', '')
        self.lock = threading.RLock()
        self.sound_client = SoundClient()
        self.publish_rate = rospy.get_param('~publish_rate', 50)
        self.sound_and_count = rospy.get_param('~sound_and_count', True)
        if self.sound_and_count:
            self.countdown_path = rospy.get_param('~countdown_path', '/home/gabry/catkin_ws/src/hmri_pt_laser/nodes/sounds/countdown.wav')
            self.beep_path  = rospy.get_param('~beep_path', '/home/gabry/catkin_ws/src/hmri_pt_laser/nodes/sounds/beep.wav')
        sec = rospy.get_param('~tf_exp_time', 90.0)
        self.tf_exp_time = rospy.Duration(sec)

        self.human_frame = rospy.get_param('~human_frame_id', 'human_footprint')
        self.robot_root_frame = rospy.get_param('~robot_root_frame', 'base_link')
        #bebop_goto_action_ns = rospy.get_param('~bebop_goto_action_ns', '/bebop/goto_action')
        #bebop_shape_action_ns = rospy.get_param('~bebop_followpath_action_ns', '/bebop/followshape_action')

        self.ray_origin_frame = rospy.get_param('~ray_origin_frame', 'eyes')
        self.ray_direction_frame = rospy.get_param('~ray_direction_frame', 'pointer')
        self.ray_inverse = rospy.get_param('~ray_inverse', False)
        self.floor = rospy.get_param('~floor', True)
        self.experiment_mode = rospy.get_param('~experiment_mode', True)
        self.shape = rospy.get_param('~shape', "8")
        self.global_state_pub = rospy.Publisher("global_state", String, queue_size=10)
        print self.experiment_mode

        """pose_topic = rospy.get_param('~robot_pose_topic', '/optitrack/bebop/pose')#'/bebop/odom/pose/pose')
                                pose_topic_class, pose_real_topic, pose_eval_func = rostopic.get_topic_class(pose_topic)
                                self.robot_pose_msg_eval = pose_eval_func
                        
                                ############ FIXME ############
                                trigger_topic = rospy.get_param('~trigger_topic', '/bebop/joy/buttons[6]')
                                trigger_topic_class, trigger_real_topic, trigger_eval_func = rostopic.get_topic_class(trigger_topic)
                                self.trigger_msg_eval = trigger_eval_func
                                # self.trigger_sub = rospy.Subscriber(trigger_real_topic, trigger_topic_class, self.trigger_cb)
                                self.trigger_val = None
                                self.last_trigger_val = None
                                ###############################"""
        self.trigger_val = None
        self.last_trigger_val = None
        self.timewindow = rospy.get_param('~timewindow', 5.0)
        self.sync_freq = rospy.get_param('~freq', 50.0)
        self.sample_size = rospy.get_param('~sample_size', 150)

        if self.timewindow and self.sync_freq:
            self.queue_size = int(self.timewindow * self.sync_freq); # 5.0 seconds at 50 Hz
            rospy.loginfo('Max queue size: {}'.format(self.queue_size))
            if self.sample_size > self.queue_size:
                rospy.loginfo('sample_size [{}] is bigger than queue_size [{}]. Setting sample_size = queue_size'.format)
                self.sample_size = self.queue_size
        else:
            rospy.logwarn('Either timewindow or queue_size is set to 0. Using unbound queue.')
            self.queue_size = None
        self.deque = collections.deque(maxlen = self.queue_size)

        self.robot_pose_msg = None
        self.robot_sub = rospy.Subscriber("target_point", PointStamped, self.robot_pose_cb)

        self.is_valid_pub = rospy.Publisher('is_relloc_valid', Bool, queue_size = 10)
        self.just_relloc = False

        if self.floor:
            self.shape_pub = rospy.Publisher("floor_target_shape", String, queue_size = 1)
        else:
            self.shape_pub = rospy.Publisher("wall_target_shape", String, queue_size = 1)
        self.set_drawing_pub = rospy.Publisher("drawing", Bool, queue_size = 1)
        self.state_pub = rospy.Publisher("state", String, queue_size = 1)
        self.initial_guess = np.array([0, 0, 0, 0])
        self.reset_state()

        self.tf_buff = tf2_ros.Buffer()
        self.tf_ls = tf2_ros.TransformListener(self.tf_buff)
        self.tf_br = tf2_ros.TransformBroadcaster()

        # self.bebop_goto_client = actionlib.SimpleActionClient(bebop_goto_action_ns, GoToAction)
        # rospy.loginfo('Waiting for ' + bebop_goto_action_ns)
        # self.bebop_goto_client.wait_for_server()
        # self.bebop_shape_client = actionlib.SimpleActionClient(bebop_shape_action_ns, FollowShapeAction)
        # rospy.loginfo('Waiting for ' + bebop_shape_action_ns)
        # self.bebop_shape_client.wait_for_server()

        self.relloc_server = actionlib.SimpleActionServer('relloc_action', MotionRellocAction, self.execute_relloc, False)
        self.relloc_server.start()

    def reset_state(self):
        with self.lock:
            self.deque.clear()
            self.estimated_tf = self.initial_guess
            self.cached_tf = None
            self.estimated = False
            self.tf_expired = True

    def robot_pose_cb(self, msg):
        self.robot_pose_msg = msg
        self.topic_sync_cb(self.robot_pose_msg)

    def execute_relloc(self, goal):
        loop_rate = rospy.Rate(50) # 50 Hz
        self.state_pub.publish("following_target")

        """if not self.robot_pose_msg:
                                    self.relloc_server.preempt_request = True
                                    self.relloc_server.set_aborted()
                                    return"""

        self.trigger_cb(True)

        """shape = FollowShapeGoal()
                                shape.type = 'regular_polygon'
                                shape.vertices = 3 # let's use triangle for now
                                shape.radius = 1.0
                                shape.fix_orientation = True
                                # Use current pose as the center
                                shape.pose = copy.deepcopy(self.robot_pose_msg)"""

        #self.bebop_shape_client.send_goal(shape)
        self.set_drawing_pub.publish(True)
        #playsound('catkin_ws/src/hmri_pt_laser/nodes/sounds/countdown.wav')
        self.global_state_pub.publish("COUNTING_10")
        if self.sound_and_count:
            self.sound_client.playWave(self.countdown_path)
            rospy.sleep(10) 
        self.shape_pub.publish(self.shape)
        rospy.sleep(1)
        self.global_state_pub.publish("RELLOC_START")
        start_stamp = rospy.Time.now()
        while not rospy.is_shutdown():
            if self.relloc_server.is_preempt_requested():
                self.trigger_cb(False)
                self.bebop_shape_client.cancel_goal()
                self.relloc_server.set_preempted()
                rospy.logwarn('Relloc action has been preempted')
                break

            with self.lock:
                cur_length = len(self.deque)

            feedback = MotionRellocFeedback()
            feedback.samples_counter = cur_length
            self.relloc_server.publish_feedback(feedback)

            if rospy.Time.now() > (start_stamp + goal.max_duration) and cur_length >= goal.min_samples:
                self.trigger_cb(False)

                with self.lock:
                    self.cached_tf = self.estimate_pose()
                    pose = PoseStamped()
                    pose.header = self.cached_tf.header
                    pose.pose.position.x = self.cached_tf.transform.translation.x
                    pose.pose.position.y = self.cached_tf.transform.translation.y
                    pose.pose.position.z = self.cached_tf.transform.translation.z
                    pose.pose.orientation.x = self.cached_tf.transform.rotation.x
                    pose.pose.orientation.y = self.cached_tf.transform.rotation.y
                    pose.pose.orientation.z = self.cached_tf.transform.rotation.z
                    pose.pose.orientation.w = self.cached_tf.transform.rotation.w

                self.set_drawing_pub.publish(False)
                #playsound('catkin_ws/src/hmri_pt_laser/nodes/sounds/beep.wav')
                if self.sound_and_count:
                    self.sound_client.playWave(self.beep_path)
                if self.experiment_mode:
                    print("here")
                    self.state_pub.publish("marking_relloc")
                else:
                    self.state_pub.publish("following_arm")
                self.relloc_server.set_succeeded(result=MotionRellocResult(pose))
                self.global_state_pub.publish("RELLOC_END")
                break

            loop_rate.sleep()

    def trigger_cb(self, msg):
        self.last_trigger_val = self.trigger_val
        self.trigger_val = msg

        # if self.trigger_msg_eval:
        #     self.trigger_val = True if self.trigger_msg_eval(msg) else False
        # else:
        #     self.trigger_val = False

        if self.last_trigger_val != self.trigger_val:
            # rospy.logwarn(self.trigger_val)
            pass

        if not self.last_trigger_val and self.trigger_val:
            rospy.loginfo('Collecting new data')
            self.reset_state()

    def estimate_pose(self):
        ########### FIXME ##############
        np.random.seed(0)
        ################################

        data = np.array(self.deque)

        sample_size = self.sample_size
        if sample_size > data.shape[0]:
            sample_size = data.shape[0]

        rospy.loginfo('Estimating pose. Using {} of total {} data points'.format(sample_size, data.shape[0]))

        idx = np.random.choice(data.shape[0], size=sample_size, replace=False)
        tmp = data[idx, :]
        p = np.transpose(tmp[:, 0:3])
        qc = np.transpose(tmp[:, 3:6])
        qv = np.transpose(tmp[:, 6:9])

        res = relloclib.estimate_pose(p, qc, qv, self.estimated_tf)
        self.estimated_tf = res.x

        rospy.loginfo("Average angular error (residual) in deg: {:.2f}".format(np.rad2deg(res.fun)))
        rospy.loginfo("Recovered transform (tx, ty, tz, rotz): {:.2f}, {:.2f}, {:.2f}, {:.2f}".format(
                    res.x[0],
                    res.x[1],
                    res.x[2],
                    np.rad2deg(res.x[3])))

        est_tran = res.x[:3]
        est_quat = kdl.Rotation.RPY(0.0, 0.0, res.x[3]).GetQuaternion()

        t = TransformStamped()

        t.header.frame_id = self.human_frame
        t.child_frame_id = self.robot_root_frame
        t.header.stamp = rospy.Time.now()
        t.transform.translation.x = est_tran[0]
        t.transform.translation.y = est_tran[1]
        t.transform.translation.z = est_tran[2]
        t.transform.rotation.x = est_quat[0]
        t.transform.rotation.y = est_quat[1]
        t.transform.rotation.z = est_quat[2]
        t.transform.rotation.w = est_quat[3]

        self.estimated = True

        return t

    def topic_sync_cb(self, robot_pose_msg):
        robot_pos = (robot_pose_msg.point.x, robot_pose_msg.point.y, robot_pose_msg.point.z)
        
        try:
            origin_tf = self.tf_buff.lookup_transform(self.human_frame, self.ray_origin_frame, rospy.Time())#robot_pose_msg.header.stamp)
            ray_tf = self.tf_buff.lookup_transform(self.human_frame, self.ray_direction_frame, rospy.Time())#robot_pose_msg.header.stamp)
            if self.ray_inverse:
                unit_vector = kdl.Vector(-1.0, 0.0, 0.0)
            else:
                unit_vector = kdl.Vector(1.0, 0.0, 0.0)
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException), e:
            rospy.logerr(e)
            return

        orig = (origin_tf.transform.translation.x,
                origin_tf.transform.translation.y,
                origin_tf.transform.translation.z)

        quat = (ray_tf.transform.rotation.x,
                ray_tf.transform.rotation.y,
                ray_tf.transform.rotation.z,
                ray_tf.transform.rotation.w)

        frame = tfc.fromTf((orig, quat))

        p = list(robot_pos)
        # q is a ray
        q = [frame.p.x(), frame.p.y(), frame.p.z()] #origin of pointer
        # Rotate unit vector in the direction of pointing
        v = frame.M * unit_vector

        q.extend([v.x(), v.y(), v.z()])
        p.extend(q)

        with self.lock:
            self.deque.append(p)

    def run(self):
        loop_rate = rospy.Rate(self.publish_rate)

        while not rospy.is_shutdown():
            try:
                now = rospy.Time.now()
                if self.cached_tf:
                    self.tf_expired = now > (self.cached_tf.header.stamp + self.tf_exp_time)

                    if not self.tf_expired:
                        t = copy.deepcopy(self.cached_tf)
                        # Update stamp to keep tf alive
                        t.header.stamp = now
                        t.transform.translation.z =0
                        self.tf_br.sendTransform(t)

                self.is_valid_pub.publish(not self.tf_expired)

                loop_rate.sleep()

            except rospy.ROSException, e:
                if e.message == 'ROS time moved backwards':
                    rospy.logwarn("Saw a negative time change. Resetting internal state...")
                    self.reset_state()

if __name__ == '__main__':
    rospy.init_node('motion_relloc')

    motion_relloc = MotionRellocNode()

    try:
        motion_relloc.run()
    except rospy.ROSInterruptException:
        rospy.logdebug('Exiting')
        pass
