#!/usr/bin/env python

import collections
import copy
import threading
import csv
from datetime import datetime

import numpy as np
from sound_play.libsoundplay import SoundClient

import rospy
import rostopic

import actionlib
from std_msgs.msg import Bool, Empty, String, Float32, Header
import tf2_ros
import tf2_geometry_msgs
import tf_conversions as tfc
import PyKDL as kdl

from geometry_msgs.msg import Pose, PoseStamped, TransformStamped, PointStamped, Point

from hmri_msgs.msg import MotionRellocAction, MotionRellocFeedback, MotionRellocResult
from hmri_msgs.msg import GoToAction, GoToGoal
from hmri_msgs.msg import FollowShapeAction, FollowShapeGoal, MotionRellocGoal

import relloclib
import tf
from scipy.ndimage.interpolation import shift

class GoalQueue:
    def __init__(self, size, threshold):
        self.tail = -1
        self.MAX_SIZE = size-1
        self.x_queue = np.zeros(size)
        self.y_queue = np.zeros(size)
        self.threshold = threshold
    def enqueue(self, x, y):
        if self.tail == self.MAX_SIZE:
            self.x_queue = shift(self.x_queue, -1, cval = x)
            self.y_queue = shift(self.y_queue, -1, cval = y)
        elif self.tail < self.MAX_SIZE:
            self.tail += 1
            self.x_queue[self.tail] = x
            self.y_queue[self.tail] = y
    def distance_from_oldest(self):
        px, py = self.x_queue[0], self.y_queue[0]
        return np.sqrt((px - self.x_queue[1:])**2 + ((py - self.y_queue[1:]))**2)

    def goal_detected(self):
        max_distance = np.max(self.distance_from_oldest())
        if max_distance <= self.threshold and self.tail == self.MAX_SIZE:
            return True
        else:
            return False
    def clear(self):
        self.tail = -1
        self.x_queue = np.zeros(self.MAX_SIZE+1)
        self.y_queue = np.zeros(self.MAX_SIZE+1)


class KobukiGoToGoalNode:
    def __init__(self):
        self.relloc_client = actionlib.SimpleActionClient('relloc_action', MotionRellocAction)
        self.relloc_client.wait_for_server()
        self.state_pub = rospy.Publisher("state", String, queue_size = 1)
        self.button_sub = rospy.Subscriber("/idsia/myo2/button", Bool, self.button_cb)
        self.kobuki_goal_pub = rospy.Publisher('/mobile_base/commands/goal_point', PointStamped, queue_size=10)
        self.kobuki_reset_odom = rospy.Publisher('/mobile_base/commands/reset_odometry', Empty, queue_size=10)
        self.pose_target_sub = rospy.Subscriber('idsia/arm_pointer', PoseStamped, self.update_go_to_point)
        self.state_pub = rospy.Publisher("state", String, queue_size=10)
        self.go_to_point = PointStamped()
        self.button_down = False
        self.last_button = False
        #self.target_pub = rospy.Publisher('target_point', PointStamped, queue_size = 10)
        self.end_of_try = False
        self.reloc_done = False
        self.state = 'reloc_not_done'
        self.tf = tf.TransformListener()
        self.goal_queue = GoalQueue(size=150, threshold=0.10)
        self.sound_client = SoundClient()
        

    def button_cb(self, msg):
        if msg.data == True and self.last_button == False:
            self.button_down = msg.data

        self.last_button = msg.data

    def update_go_to_point(self, msg):
        if self.state == 'wait_goal_point':
            self.goal_queue.enqueue(msg.pose.position.x, msg.pose.position.y)
            if self.goal_queue.goal_detected():
                stamped_point = PointStamped()
                stamped_point.header = msg.header
                stamped_point.header.stamp = rospy.Time() #= msg.header
                stamped_point.point = msg.pose.position
                self.go_to_point = stamped_point#self.tf.transformPoint("base_link", stamped_point)
                self.state = 'goal_point_acquired'
                self.goal_queue.clear()

    def run(self):
        while not rospy.is_shutdown():
            try:
                print("Press the button to go\n")
                while not self.button_down:
                    pass
                if self.button_down:
                    self.button_down = False
                    self.kobuki_reset_odom.publish()
                    goal = MotionRellocGoal()
                    goal.max_duration = rospy.rostime.Duration(5) #(secs=5, nsecs=0)
                    goal.min_samples = 250
                    self.relloc_client.send_goal_and_wait(goal)
                    self.sound_client.playWave('/home/gabry/catkin_ws/src/hmri_pt_laser/nodes/sounds/beep.wav')
                    #self.reloc_done = True
                    self.state = 'wait_goal_point'
                '''while not self.button_down:
                    pass
                if self.button_down:
                    self.button_down = False
                    self.state = 'wait_goal_point'
                '''
                while not self.state == 'goal_point_acquired':
                    pass
                if self.state == 'goal_point_acquired':
                    self.sound_client.playWave('/home/gabry/catkin_ws/src/hmri_pt_laser/nodes/sounds/beep.wav')
                    self.state = 'moving'
                    #self.reloc_done = False
                    #stamped_point = self.tf.transformPoint("base_link", self.go_to_point)
                    self.kobuki_goal_pub.publish(self.go_to_point)
                    self.state_pub.publish("marking_kobuki_goal")



            except rospy.ROSException, e:
                if e.message == 'ROS time moved backwards':
                    rospy.logwarn("Saw a negative time change. Resetting internal state...")
                    self.reset_state()
        print("End")
        
if __name__ == '__main__':
    rospy.init_node('kgtg_node')

    kgtg = KobukiGoToGoalNode()

    try:
        kgtg.run()
    except rospy.ROSInterruptException:
        rospy.logdebug('Exiting')
        pass
