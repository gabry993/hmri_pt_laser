#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import tf
import tf_conversions as tfc
from geometry_msgs.msg import PointStamped, TransformStamped, Pose, PoseStamped, Point
from std_msgs.msg import Bool, String
from visualization_msgs.msg import Marker
import math
import numpy as np
import PyKDL as kdl
#28mm from plate to axis
dz_ground = 0.59 # 72.5cm
dz_to_motor_1_flange= dz_ground + 0.04 + 0.012# 4cm
dz_to_motor_2_flange= 0.0425 # 4.25cm
class tfTurret():

    def __init__(self):
        # Initialize new node
        rospy.init_node('tf_turret', anonymous=True)
        self.rate = rospy.Rate(50) # 50hz
        self.target_sub = rospy.Subscriber('target_point', PointStamped, self.update_target_point)
        self.joint_state_pub = rospy.Publisher('joint_state', JointState, queue_size=10)
        self.br = tf.TransformBroadcaster()
        self.listener = tf.TransformListener()
        self.target_point = PointStamped()
        self.last_target_point = PointStamped()
        self.pan_angle = 0.0
        self.tilt_angle = 0.0
        self.tf = tf.TransformListener()
        self.marker_pub = rospy.Publisher('laser_marker', Marker, queue_size=10)
        self.marker_id = 0
        self.pose_target_sub = rospy.Subscriber('idsia/arm_pointer', PoseStamped, self.update_target_pose_point)
        self.state_sub = rospy.Subscriber("state", String, self.set_state_cb)
        self.relloc_human_pos_pub = rospy.Publisher('relloc_human_pos', PointStamped, queue_size=10)
        self.state = "following_target"

    def set_state_cb(self, msg):
        self.state = msg.data
        print(self.state)
    def publish_tf_tree(self, m1, m2):
        self.br.sendTransform((0, 0, dz_to_motor_1_flange),
                         tf.transformations.quaternion_from_euler(0, 0, m1),
                         rospy.Time.now(),
                         "pan_link",
                         "base_link")
        self.br.sendTransform((0, 0, dz_to_motor_2_flange),
                         tf.transformations.quaternion_from_euler(np.pi/2, -np.pi/2, 0),
                         rospy.Time.now(),
                         "tilt_link",
                         "pan_link")
        self.br.sendTransform((0, 0, 0),
                         tf.transformations.quaternion_from_euler(0, 0, m2 - np.pi/2),
                         rospy.Time.now(),
                         "laser_link",
                         "tilt_link")
        self.br.sendTransform((5.0, 0, 0),
                         tf.transformations.quaternion_from_euler(0, 0, 0),
                         rospy.Time.now(),
                         "debug_link",
                         "laser_link")
        self.rate.sleep()



    def run(self):
        while not rospy.is_shutdown():
            self.publish_tf_tree(self.pan_angle, self.tilt_angle)

    def update_target_pose_point(self, msg):
        if self.state == "following_arm":
            self.stamped_point = PointStamped()
            self.stamped_point.header = msg.header
            self.stamped_point.header.stamp = rospy.Time() #= msg.header
            self.stamped_point.point = msg.pose.position
            print(self.stamped_point)
            self.target_point = self.tf.transformPoint("base_link", self.stamped_point)
            #print(self.target_point)
            #self.target_point = msg.pose.position
            self.pan_angle, self.tilt_angle = self.ik(self.target_point.point.x, self.target_point.point.y, self.target_point.point.z)
            self.br.sendTransform((self.target_point.point.x, self.target_point.point.y, self.target_point.point.z),
                             tf.transformations.quaternion_from_euler(0, 0, 0),
                             rospy.Time.now(),
                             "debug_point",
                             "base_link")
            laser_marker = Marker()
            laser_marker.header.frame_id = "/base_link"
            laser_marker.header.stamp    = rospy.get_rostime()
            laser_marker.ns = "robot"
            laser_marker.id = self.marker_id
            self.marker_id +=1
            pose = Pose()
            pose.position = self.target_point
            laser_marker.pose.position = self.target_point.point
            laser_marker.scale.x = 0.01
            laser_marker.scale.y = 0.01
            laser_marker.scale.z = 0.01

            laser_marker.color.r = 0.0
            laser_marker.color.g = 1.0
            laser_marker.color.b = 0.0
            laser_marker.color.a = 1.0

            laser_marker.lifetime = rospy.Duration(10)
            self.publish_joint_state(self.pan_angle, self.tilt_angle)
            self.marker_pub.publish(laser_marker)
        elif self.state == "marking_relloc":
            try:
                (trans,rot) = self.listener.lookupTransform('base_link', 'human_footprint', rospy.Time(0))
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                pass
            self.target_point.point.x = trans[0]
            self.target_point.point.y = trans[1]
            self.target_point.point.z = trans[2]
            self.relloc_human_pos_pub.publish(self.target_point)
            self.pan_angle, self.tilt_angle = self.ik(self.target_point.point.x, self.target_point.point.y, self.target_point.point.z)
            self.br.sendTransform((self.target_point.point.x, self.target_point.point.y, self.target_point.point.z),
                             tf.transformations.quaternion_from_euler(0, 0, 0),
                             rospy.Time.now(),
                             "debug_point",
                             "base_link")
            laser_marker = Marker()
            laser_marker.header.frame_id = "/base_link"
            laser_marker.header.stamp    = rospy.get_rostime()
            laser_marker.ns = "robot"
            laser_marker.id = self.marker_id
            self.marker_id +=1
            pose = Pose()
            pose.position = self.target_point
            laser_marker.pose.position = self.target_point.point
            laser_marker.scale.x = 0.01
            laser_marker.scale.y = 0.01
            laser_marker.scale.z = 0.01

            laser_marker.color.r = 0.0
            laser_marker.color.g = 1.0
            laser_marker.color.b = 0.0
            laser_marker.color.a = 1.0

            laser_marker.lifetime = rospy.Duration(10)
            self.publish_joint_state(self.pan_angle, self.tilt_angle)
            self.marker_pub.publish(laser_marker)

    def update_target_point(self, msg):
        if self.state =="following_target":
            self.target_point = msg
            self.pan_angle, self.tilt_angle = self.ik(msg.point.x, msg.point.y, msg.point.z)
            self.br.sendTransform((msg.point.x, msg.point.y, msg.point.z),
                             tf.transformations.quaternion_from_euler(0, 0, 0),
                             rospy.Time.now(),
                             "debug_point",
                             "base_link")
            laser_marker = Marker()
            laser_marker.header.frame_id = "/base_link"
            laser_marker.header.stamp    = rospy.get_rostime()
            laser_marker.ns = "robot"
            laser_marker.id = self.marker_id
            self.marker_id +=1
            pose = Pose()
            pose.position = msg.point
            laser_marker.pose.position = msg.point
            laser_marker.scale.x = 0.01
            laser_marker.scale.y = 0.01
            laser_marker.scale.z = 0.01

            laser_marker.color.r = 0.0
            laser_marker.color.g = 1.0
            laser_marker.color.b = 0.0
            laser_marker.color.a = 1.0

            laser_marker.lifetime = rospy.Duration(10)
            self.publish_joint_state(self.pan_angle, self.tilt_angle)
            self.marker_pub.publish(laser_marker)
        

    def publish_joint_state(self, pan, tilt):
        joint_state_msg = JointState()
        joint_state_msg.header = Header()
        joint_state_msg.header.stamp = rospy.Time.now()
        joint_state_msg.name = ['pan_link', 'laser_link']
        joint_state_msg.position = [math.degrees(pan), math.degrees(tilt - np.pi/2)]
        joint_state_msg.velocity = []
        joint_state_msg.effort = []
        self.joint_state_pub.publish(joint_state_msg)

    def ik(self, x, y, z):
        pan = np.math.atan2(y, x)
        dz = dz_to_motor_1_flange + dz_to_motor_2_flange - z
        tilt = -np.math.atan2(dz, np.math.sqrt(x**2 + y**2))
        return pan, tilt

        # (m1_pos, m2_pos) = self.turret_ik(**msg.position)


if __name__ == '__main__':
    try:
        turret = tfTurret()
        turret.run()
    except rospy.ROSInterruptException:
        pass