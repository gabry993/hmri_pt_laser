#!/usr/bin/env python

import collections
import copy
import threading
import csv
from datetime import datetime

import numpy as np
from sound_play.libsoundplay import SoundClient

import rospy
import rostopic

import actionlib
from std_msgs.msg import Bool, Empty, String, Float32, Header
import tf2_ros
import tf2_geometry_msgs
import tf_conversions as tfc
import PyKDL as kdl

from geometry_msgs.msg import Pose, PoseStamped, TransformStamped, PointStamped, Point

from hmri_msgs.msg import MotionRellocAction, MotionRellocFeedback, MotionRellocResult
from hmri_msgs.msg import GoToAction, GoToGoal
from hmri_msgs.msg import FollowShapeAction, FollowShapeGoal, MotionRellocGoal

import relloclib

class TryNode:
    def __init__(self):
        self.relloc_client = actionlib.SimpleActionClient('relloc_action', MotionRellocAction)
        self.relloc_client.wait_for_server()
        self.state_pub = rospy.Publisher("state", String, queue_size = 1)
        self.button_sub = rospy.Subscriber("/idsia/myo2/button", Bool, self.button_cb)
        self.button_down = False
        self.last_button = False
        #self.target_pub = rospy.Publisher('target_point', PointStamped, queue_size = 10)
        self.end_of_try = False

    def button_cb(self, msg):
        if msg.data == True and self.last_button == False:
            self.button_down = msg.data

        self.last_button = msg.data

    def run(self):
        while not rospy.is_shutdown():
            try:
                print("Press the button to go\n")
                while not self.button_down:
                    rospy.sleep(0.1)
                if self.button_down:
                    self.button_down = False
                    goal = MotionRellocGoal()
                    goal.max_duration = rospy.rostime.Duration(5) #(secs=5, nsecs=0)
                    goal.min_samples = 250
                    self.relloc_client.send_goal_and_wait(goal)
                    self.state_pub.publish("following_arm")
                    #rospy.sleep(2)
            except rospy.ROSException, e:
                if e.message == 'ROS time moved backwards':
                    rospy.logwarn("Saw a negative time change. Resetting internal state...")
                    self.reset_state()
        print("End")
        
if __name__ == '__main__':
    rospy.init_node('experiment')

    experiment = TryNode()

    try:
        experiment.run()
    except rospy.ROSInterruptException:
        rospy.logdebug('Exiting')
        pass
