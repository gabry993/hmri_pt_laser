#!/usr/bin/env python

import math, copy, random
import numpy as np

import rospy, rospkg

from std_msgs.msg import Bool, Empty, String, Float32, Float32MultiArray
from geometry_msgs.msg import PoseStamped, Pose
from sensor_msgs.msg import JointState

import dynamic_reconfigure.server as dr

import tf2_ros
import tf2_geometry_msgs
import tf_conversions as tfc
import PyKDL as kdl

from bioloid.serial_bus import SerialPort, SerialBus
from bioloid.device_type_parser import DeviceTypeParser
from bioloid.device_type import DeviceTypes
from bioloid.device import Device
from bioloid.bus import BusError
from serial import SerialException

class dynamixelTurret:
    def __init__(self):
        self.publish_rate = rospy.get_param('~publish_rate', 5)

        self.initialized = False
        self.compliance_slope = 128
        self.compliance_margin = 0
        self.speed= 30

        self.initialized = self.init_devices()

        self.joint_state_sub = rospy.Subscriber('joint_state', JointState, self.set_joint_angle)

        self.hz =rospy.get_param("~frequency", 50)
        self.step= 10
        self.rate = rospy.Rate(self.hz)

    def init_devices(self):
        baud_rate = rospy.get_param('~baud_rate', 1000000)
        port = rospy.get_param('~port', '/dev/ttyUSB0')

        pkg = rospkg.RosPack()
        dev_conf_path = pkg.get_path('hmri_pt_laser') + '/devcfg'

        self.joint_ids = rospy.get_param('~joint_ids', [1, 2])
        self.zeros = {}
        self.zeros[self.joint_ids[0]] = 150.0
        self.zeros[self.joint_ids[1]] = 150.0

        try:
            serial_port = SerialPort(port = port, baudrate = baud_rate)
        except SerialException:
            rospy.logfatal('Unable to open \'{}\''.format(port))
            return False

        self.bus = SerialBus(serial_port, show_packets = True)

        dev_types = DeviceTypes()
        parser = DeviceTypeParser(dev_types)
        parser.parse_dev_type_files(dev_conf_path)
        servo_type = dev_types.get('servo')
        if not servo_type:
            rospy.logfatal('Unable to find device type \'servo\'')
            return False

        self.dev = {}
        self.led = {}
        self.vel = {}
        self.pos = {}
        self.ccw_lim = {}
        self.cw_lim = {}
        self.cur_pos = {}
        self.cw_comp_slope = {}
        self.ccw_comp_slope = {}
        self.cw_comp_margin = {}
        self.ccw_comp_margin = {}

        for i in self.joint_ids:
            self.dev[i] = Device(self.bus, i, servo_type)

            ping = {}
            while not self.dev[i].ping():
                rospy.logerr('Device {} is not responding. Will try again later'.format(i))
                rospy.sleep(0.5)

            self.led[i] = self.dev[i].get_dev_reg('led')
            self.vel[i] = self.dev[i].get_dev_reg('moving-speed')
            self.pos[i] = self.dev[i].get_dev_reg('goal-position')
            self.cur_pos[i] = self.dev[i].get_dev_reg('present-position')

            self.ccw_lim[i] = self.dev[i].get_dev_reg('ccw-angle-limit')
            self.cw_lim[i] = self.dev[i].get_dev_reg('cw-angle-limit')
            if i==1 and self.cw_lim[i].get() != 60.0:
                self.cw_lim[i].set(60.0)
                self.ccw_lim[i].set(240.0)
            if i==2 and self.cw_lim[i].get() != 15.0:
                self.cw_lim[i].set(15.0)
                self.ccw_lim[i].set(150.0)
            #rospy.loginfo('J{} pos: {} lim: ({}, {})'.format(i, self.pos[i].get() - self.zeros[i], self.cw_lim[i].get(), self.ccw_lim[i].get()))
            rospy.loginfo('J{} pos: {} lim: ({}, {})'.format(i, self.pos[i].get() - self.zeros[i], self.cw_lim[i].get() - self.zeros[i], self.ccw_lim[i].get() - self.zeros[i]))

            
            self.cw_comp_slope[i] = self.dev[i].get_dev_reg('cw-comp-slope')
            self.ccw_comp_slope[i] = self.dev[i].get_dev_reg('ccw-comp-slope')
            self.cw_comp_slope[i].set(self.compliance_slope)
            self.ccw_comp_slope[i].set(self.compliance_slope)

            self.cw_comp_margin[i] = self.dev[i].get_dev_reg('cw-comp-margin')
            self.ccw_comp_margin[i] = self.dev[i].get_dev_reg('ccw-comp-margin')
            self.cw_comp_margin[i].set(self.compliance_margin)
            self.ccw_comp_margin[i].set(self.compliance_margin)
            self.vel[i].set(self.speed)
            self.bus.send_action()
            rospy.loginfo('J{} speed: {}'.format(i, self.vel[i].get()))
            rospy.loginfo('J{} cw slope: {} ccw slope: {}'.format(i, self.cw_comp_slope[i].get(), self.ccw_comp_slope[i].get()))
            rospy.loginfo('J{} cw margin: {} ccw margin: {}'.format(i, self.cw_comp_margin[i].get(), self.ccw_comp_margin[i].get()))
        return True

    def set_joint_angle(self, msg):
        if not self.initialized:
            return
        angles = msg.position
        
        new_pos = {}
        new_pos_print = {}
        vel = {}
        
        # > 360
        max_dist = 0 # deg

        max_vel = 114.0 # RPM
        max_t = 0 # sec

        for idx, i in enumerate(self.joint_ids):
            new_pos[i] = angles[idx] + self.zeros[i]
            new_pos_print[i] = angles[idx]
            '''dist = math.fabs(self.cur_pos[i].get() - new_pos[i])
            if dist > max_dist:
                max_dist = dist'''

        max_t = max_dist / (114.0)

        #rospy.loginfo('max_t: {}'.format(max_t))

        '''for i in self.joint_ids:
            if max_t:
                vel[i] = math.fabs(self.cur_pos[i].get() - new_pos[i]) / max_t
            else:
                vel[i] = 0.0'''

        #rospy.loginfo('new_pos: {}'.format(new_pos_print))
        #rospy.loginfo('Vel: {}'.format(vel))

        for i in self.joint_ids:
            #self.led[i].set(0)
            is_error = False
            try:
                #self.vel[i].set(60.0)#self.vel[i].set(vel[i])
                self.pos[i].set(new_pos[i], deferred = True)

            except BusError as e:
                # Set LED
                self.led[i].set(1)
                # Don't move
                self.pos[i].set(self.cur_pos[i].get())
                rospy.logerr_throttle(0.5, e)
                is_error = True

        if not is_error:
            self.bus.send_action()
            return
    """def fk(self, base_angle, upper_angle):
                    dist_ground_end = h/math.cos(upper_angle)
                    base_i = np.sqrt(dist_ground_end**2 - h**2)
                    x = base_i*math.cos(base_angle)
                    y = base_i*math.sin(base_angle)
                    return x,y
                
                def ik(self, x, y):
                    wd = self.distance_from_wall
                    #if x==0.0:
                    #    base_angle = 90.0
                    #else:
                    base_angle = 90+np.degrees(np.arctan2(-x,wd))
                    i=np.sqrt(wd**2 + x**2)
                    upper_angle = np.degrees(np.arctan2(y,i))
                    rospy.loginfo('base_angle: {}'.format(base_angle))
                    rospy.loginfo('upper_angle: {}'.format(upper_angle))
                    return base_angle, upper_angle"""

if __name__ == '__main__':
    rospy.init_node('pt_laser', anonymous = False)
    pt_laser = dynamixelTurret()
    rospy.spin()
