#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy

# Author: Andrew Dai
# This ROS Node converts Joystick inputs from the joy node
# into commands for turtlesim

# Receives joystick messages (subscribed to Joy topic)
# then converts the joysick inputs into Twist commands
# axis 1 aka left stick vertical controls linear speed
# axis 0 aka left stick horizonal controls angular speed
class Joystick():
    def __init__(self):
        self.pub = rospy.Publisher('laser_dot/velocity', Twist, queue_size = 1)
        self.sub = rospy.Subscriber("joy", Joy, self.callback)

    def callback(self, data):
        twist = Twist()
        twist.linear.x = data.axes[0]
        twist.linear.y = data.axes[1]
        self.pub.publish(twist)


if __name__ == '__main__':
    rospy.init_node('JoyNode')
    j = Joystick()
    rospy.spin()