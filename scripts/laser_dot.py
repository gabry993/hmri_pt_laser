#!/usr/bin/env python

import math, copy, random
import numpy as np

import rospy, rospkg

from std_msgs.msg import Bool, Empty, String, Float32, Float32MultiArray
from geometry_msgs.msg import PoseStamped, Twist, Pose, Point
class LaserDot():
    def __init__(self):

        self.pose_publisher = rospy.Publisher('/laser_dot/pose', Pose, queue_size=10)
        self.velocity_subscriber = rospy.Subscriber('/laser_dot/velocity', Twist, self.update_speed)
        self.pose = Pose()
        self.pose.position.x = 0
        self.pose.position.y = 0
        self.x_speed = 0
        self.y_speed = 0
        self.hz =50.0
        self.rate = rospy.Rate(self.hz)

    def update_speed(self, data):
        self.x_speed = -data.linear.x
        self.y_speed = data.linear.y
    def run(self):
        while not rospy.is_shutdown():
            try:
                self.pose_publisher.publish(self.pose)
                self.pose.position.x += 5*self.x_speed 
                self.pose.position.y += 5*self.y_speed
                self.rate.sleep()
            except rospy.ROSException, e:
                if e.message == 'ROS time moved backwards':
                    rospy.logwarn("Saw a negative time change, resetting.")

if __name__ == '__main__':
    rospy.init_node('laser_dot', anonymous = False)

    laser_dot = LaserDot()

    try:
        laser_dot.run()
    except rospy.ROSInterruptException:
        rospy.logdebug('Exiting')
        pass
