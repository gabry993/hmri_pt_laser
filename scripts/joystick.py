#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist, PointStamped
from std_msgs.msg import Header
from sensor_msgs.msg import Joy

# Author: Andrew Dai
# This ROS Node converts Joystick inputs from the joy node
# into commands for turtlesim

# Receives joystick messages (subscribed to Joy topic)
# then converts the joysick inputs into Twist commands
# axis 1 aka left stick vertical controls linear speed
# axis 0 aka left stick horizonal controls angular speed
class Joystick():
    def __init__(self):
        self.target_pub = rospy.Publisher('target_point', PointStamped, queue_size = 1)
        self.sub = rospy.Subscriber("joy", Joy, self.callback)
        self.horizontal = 0.0
        self.vertical = 0.0
        self.laser_dot = PointStamped()
        self.hz =20.0
        self.rate = rospy.Rate(self.hz)
        self.base = rospy.get_param("~base", "wall")

    def callback(self, data):
        self.horizontal = data.axes[0]
        self.vertical = data.axes[1]
    
    def run(self):
        while not rospy.is_shutdown():
            try:
                if self.base == "floor":
                    self.laser_dot.header = Header()
                    self.laser_dot.header.stamp = rospy.Time.now()
                    self.laser_dot.point.x += 0.1*self.vertical
                    self.laser_dot.point.y += 0.1*self.horizontal
                    #print("x "+ str(self.laser_dot.point.x))
                    #print("y "+ str(self.laser_dot.point.y))
                    self.laser_dot.point.z = 0
                    self.target_pub.publish(self.laser_dot)
                elif self.base == "wall":
                    self.laser_dot.header = Header()
                    self.laser_dot.header.stamp = rospy.Time.now()
                    self.laser_dot.point.x = 1
                    self.laser_dot.point.y += 0.1*self.horizontal
                    self.laser_dot.point.z += 0.1*self.vertical
                    #print("y "+ str(self.laser_dot.point.y))
                    #print("z "+ str(self.laser_dot.point.z))
                    
                    self.target_pub.publish(self.laser_dot)
                self.rate.sleep()
            except rospy.ROSException, e:
                if e.message == 'ROS time moved backwards':
                    rospy.logwarn("Saw a negative time change, resetting.")
if __name__ == '__main__':
    rospy.init_node('JoyNode')
    j = Joystick()
    try:
        j.run()
    except rospy.ROSInterruptException:
        rospy.logdebug('Exiting')
        pass