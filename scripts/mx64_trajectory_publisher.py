#!/usr/bin/env python

import math, copy, random
import numpy as np

import rospy, rospkg

from std_msgs.msg import Bool, Empty, String, Float32, Header
from geometry_msgs.msg import PoseStamped, Pose, PointStamped
from sensor_msgs.msg import JointState

import dynamic_reconfigure.server as dr

import tf2_ros
import tf2_geometry_msgs
import tf_conversions as tfc
import PyKDL as kdl

class trajectoryPublisher:
    def __init__(self):
        self.publish_rate = rospy.get_param('~publish_rate', 5)

        self.floor_action_sub = rospy.Subscriber('floor_target_shape', String, self.run_floor_trajectory)
        self.wall_action_sub = rospy.Subscriber('wall_target_shape', String, self.run_wall_trajectory)
        self.set_drawing_sub = rospy.Subscriber('drawing', Bool, self.set_drawing_cb)
        self.target_pub = rospy.Publisher('target_point', PointStamped, queue_size = 10)
        

        self.shape = rospy.get_param("~shape", "square")
        self.floor = rospy.get_param("~floor", True)
        self.distance_from_wall = rospy.get_param("~distance_from_wall", 1.5)

        self.drawing = False
        self.hz =rospy.get_param("~frequency", 50)
        self.step = 1
        self.rate = rospy.Rate(self.hz)

        self.inf_center = 70.0
        self.x_center = 190.0
        self.y_center = 0.0
        self.radius = 70.0

    def set_drawing_cb(self, msg):
        self.drawing = msg.data

    def run(self):
        while not rospy.is_shutdown():
            if self.floor:
                self.run_floor_trajectory()
            else:
                self.run_wall_trajectory()

    def run_wall_trajectory(self, shape):
        while not rospy.is_shutdown() and self.drawing:
            if shape.data == 'square':
                A = [20, 20]
                B = [60, 20]
                C = [60, -20]
                D = [20, -20]
                points = []
                step = 1
                for i in range(A[0], B[0], step):
                    points.append([i, B[1]])
                for i in range(B[1], C[1], -step):
                    points.append([B[0], i])
                for i in range(C[0], D[0], -step):
                    points.append([i, C[1]])
                for i in range(D[1], A[1], step):
                    points.append([D[0], i])
                points.append(A)
                for point in points:
                    if not rospy.is_shutdown() and self.drawing:
                        msg_point = PointStamped()
                        msg_point.header = Header()
                        msg_point.header.stamp = rospy.Time.now()
                        msg_point.point.x = self.distance_from_wall
                        msg_point.point.y = point[1]/100.0
                        msg_point.point.z = point[0]/100.0
                        self.target_pub.publish(msg_point)
                        self.rate.sleep()
                    else:
                        break
            elif shape.data == "8":
                t=-90
                while t<1000:
                    if not rospy.is_shutdown() and self.drawing:
                        rad = math.radians(t)
                        scale = 2 / (3 - math.cos(2*rad));
                        x = 100*scale * math.cos(rad);
                        y = 60+(100*scale * math.sin(2*rad) / 2);
                        #rospy.loginfo("(x, y): (%.5f, %.5f) " % (x, y))
                        msg_point = PointStamped()
                        msg_point.header = Header()
                        msg_point.header.stamp = rospy.Time.now()
                        msg_point.point.x = self.distance_from_wall
                        msg_point.point.y = -x/100.0
                        msg_point.point.z = y/100.0
                        self.target_pub.publish(msg_point)
                        t+=self.step
                        self.rate.sleep()
                    else:
                        break

    def run_floor_trajectory(self, shape):
        while not rospy.is_shutdown() and self.drawing:
            if shape.data == 'square':
                A = [70, 100]
                B = [220, 100]
                C = [220, -50]
                D = [70, -50]
                points = []
                step = 1
                for i in range(A[0], B[0], step):
                    points.append([i, B[1]])
                for i in range(B[1], C[1], -step):
                    points.append([B[0], i])
                for i in range(C[0], D[0], -step):
                    points.append([i, C[1]])
                for i in range(D[1], A[1], step):
                    points.append([D[0], i])
                points.append(A)
                for point in points:
                    if not rospy.is_shutdown() and self.drawing:
                        msg_point = PointStamped()
                        msg_point.header = Header()
                        msg_point.header.stamp = rospy.Time.now()
                        msg_point.point.x = point[0]/100.0
                        msg_point.point.y = point[1]/100.0
                        msg_point.point.z = 0.0
                        self.target_pub.publish(msg_point)
                        self.rate.sleep()
                    else:
                        break
            elif shape.data == "8":
                t=-90
                while t<1000:
                    if not rospy.is_shutdown() and self.drawing:
                        rad = math.radians(t)
                        scale = 2 / (3 - math.cos(2*rad));
                        y = 100*scale * math.cos(rad);
                        x = self.inf_center+(100*scale * math.sin(2*rad) / 2);
                        #rospy.loginfo("(x, y): (%.5f, %.5f) " % (x, y))
                        msg_point = PointStamped()
                        msg_point.header = Header()
                        msg_point.header.stamp = rospy.Time.now()
                        msg_point.point.x = x/100.0
                        msg_point.point.y = -y/100.0
                        msg_point.point.z = 0.0
                        self.target_pub.publish(msg_point)
                        t+=self.step
                        self.rate.sleep()
                    else:
                        break
            elif shape.data == "ellipse":
                self.step=2
                self.x_center = 120.0
                self.y_center = 0.0
                a = 40.0
                b = 90.0
                
                t= 0
                while t<10000:
                    if not rospy.is_shutdown() and self.drawing:
                        rad = math.radians(t)
                        y = self.y_center + b*math.cos(rad);
                        x = self.x_center + a*math.sin(rad);
                        #rospy.loginfo("(x, y): (%.5f, %.5f) " % (x, y))
                        msg_point = PointStamped()
                        msg_point.header = Header()
                        msg_point.header.stamp = rospy.Time.now()
                        msg_point.point.x = x/100.0
                        msg_point.point.y = -y/100.0
                        msg_point.point.z = 0.0
                        self.target_pub.publish(msg_point)
                        t+=self.step
                        self.rate.sleep()
                    else:
                        break
            elif shape.data == "b_circle":
                self.step=1
                self.hz=100
                self.rate=rospy.Rate(self.hz)
                self.x_center = 150.0
                self.y_center = 0.0
                self.radius = 70.0
                t= 0
                while t<10000:
                    if not rospy.is_shutdown() and self.drawing:
                        rad = math.radians(t)
                        y = self.y_center + self.radius*math.cos(rad);
                        x = self.x_center + self.radius*math.sin(rad);
                        rospy.loginfo("(x, y): (%.5f, %.5f) " % (x, y))
                        msg_point = PointStamped()
                        msg_point.header = Header()
                        msg_point.header.stamp = rospy.Time.now()
                        msg_point.point.x = x/100.0
                        msg_point.point.y = -y/100.0
                        msg_point.point.z = 0.0
                        self.target_pub.publish(msg_point)
                        t+=self.step
                        self.rate.sleep()
                    else:
                        break
            elif shape.data == "s_circle":
                self.step=2
                self.hz=50
                self.rate=rospy.Rate(self.hz)
                self.x_center = 130.0
                self.y_center = 0.0
                self.radius = 40.0
                t= 0
                while t<10000:
                    if not rospy.is_shutdown() and self.drawing:
                        rad = math.radians(t)
                        y = self.y_center + self.radius*math.cos(rad);
                        x = self.x_center + self.radius*math.sin(rad);
                        rospy.loginfo("(x, y): (%.5f, %.5f) " % (x, y))
                        msg_point = PointStamped()
                        msg_point.header = Header()
                        msg_point.header.stamp = rospy.Time.now()
                        msg_point.point.x = x/100.0
                        msg_point.point.y = -y/100.0
                        msg_point.point.z = 0.0
                        self.target_pub.publish(msg_point)
                        t+=self.step
                        self.rate.sleep()
                    else:
                        break
if __name__ == '__main__':
    try:
        rospy.init_node('trajectory_publisher', anonymous = False)
        tr_pub = trajectoryPublisher()
        #tr_pub.run()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass