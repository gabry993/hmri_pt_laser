#!/usr/bin/env python

import os
import traceback
import glob
import rosbag
import rosbag_pandas_fix as rosbag_pandas
import pandas as pd
import numpy as np

from tqdm import tqdm

class DataReader():
    def __init__(self, bags_dir='bags', glob_mask='motion_relloc*.bag'):
        if not os.path.isabs(bags_dir):
            cwd = os.getcwd()
            bags_dir = os.path.join(cwd, bags_dir)

        if not os.path.exists(bags_dir):
            print('Directiory \'{}\' does not exist'.format(bags_dir))
            exit(1)

        self.all_bag_files = glob.glob(bags_dir + '/' + glob_mask)

        # ROS topics we are intrested in
        self.pointing_topics = ['/idsia/arm_pointer', '/idsia/arm_gesture']

        self.rename_topics = {'idsia_arm_pointer__pose_position_x': 'px',
                              'idsia_arm_pointer__pose_position_y': 'py',
                              'idsia_arm_pointer__pose_position_z': 'pz',
                              'idsia_arm_gesture__direction_orientation_x': 'qx',
                              'idsia_arm_gesture__direction_orientation_y': 'qy',
                              'idsia_arm_gesture__direction_orientation_z': 'qz',
                              'idsia_arm_gesture__direction_orientation_w': 'qw',

                              'target_point__point_x': 'laser_x',
                              'target_point__point_y': 'laser_y',
                              'target_point__point_z': 'laser_z'#,

                              #'bebop_joy__buttons6': 'button',
                              #'optitrack_bebop__pose_position_x': 'gt_x',
                              #'optitrack_bebop__pose_position_y': 'gt_y',
                              #'optitrack_bebop__pose_position_z': 'gt_z',
                              #'optitrack_head__pose_position_x': 'h_x',
                              #'optitrack_head__pose_position_y': 'h_y',
                              #'optitrack_head__pose_position_z': 'h_z'
                             }
    def read_joy_from_bag(self, bag_file, topics, button_index):
        ts = []
        data = []

        with rosbag.Bag(bag_file) as bag:
            for topic, msg, t in bag.read_messages(topics=topics):
                #print(msg)
                ts.append(t.to_nsec())
                # ts.append(msg.header.stamp.secs*1000000000 + msg.header.stamp.nsecs)
                data.append(msg.buttons[button_index])
        df = pd.DataFrame(index=pd.to_datetime(ts, unit="ns"), data=data)
        #df.loc[:,"diff"] = (df.diff()==0).all(axis=1)
        return df.loc[~(df.diff() == 0).all(axis=1), :][1:]

    def bags_to_dataframe(self, bags):
        # Should be like-a-list
        assert isinstance(bags, (list, str))

        # If it is a single path, convert it to a list
        if isinstance(bags, str):
            bags = [bags]

        dframes = []
        # Iterate the list
        for bag in tqdm(bags):
            try:
                # Since all the data streams (topics) are asynchronous, i.e. have
                # no common time stamps (indexes), we need to read them separately
                # to avoid None / NaN values in the matrix.
                #
                # This is potentially slow

                laser_pos = rosbag_pandas.bag_to_dataframe(bag, include=['/target_point'])
                #optitrack_head = rosbag_pandas.bag_to_dataframe(bag, include=['/optitrack/head'])
                #odom_pos      = rosbag_pandas.bag_to_dataframe(bag, include=['/bebop/odom'])
                pointed_dir   = rosbag_pandas.bag_to_dataframe(bag, include=['/idsia/arm_gesture'])
                global_state = rosbag_pandas.bag_to_dataframe(bag, include=['/global_state'])
                #buttons       = rosbag_pandas.bag_to_dataframe(bag, include=['/bebop/joy'])
                # buttons       = self.read_joy_from_bag(bag, ['/bebop/joy'], button_index=6)

                # buttons.columns = ['bebop_joy__buttons']

                df = pd.DataFrame() #optitrack_pos

                # Sync data
                for tmp in [optitrack_pos, optitrack_head, pointed_dir, buttons, odom_pos]:
                    # Skip empty
                    if not len(tmp):
                        continue
                    tmp = tmp.reindex(odom_pos.index, method='nearest')
                    df = pd.concat([df, tmp], axis=1)

                # rows = df.loc[df.index.to_series().between(buttons.index[0], buttons.index[1])]
                # df = rows

                # df = df.reindex(optitrack_pos.index, method='nearest')

                # Drop non-related data
                del_cols = list(set(df.columns) - set(self.rename_topics.keys()))
                df.drop(del_cols, axis=1, inplace=True)
                # Rename the rest
                df.rename(columns=self.rename_topics, inplace=True)

                # Make sure the data is sorted by index (time stamp)
                if not df.index.is_monotonic_increasing:
                    df = df.sort_index()

                # Add metadata to dataframe
                df.bag_file = bag
                df._metadata.append('bag_file')
                # List of data frames
                dframes.append(df)
            except Exception as e:
                print('Failed to process input bag-file: {}'.format(bag))
                traceback.print_exc()
                raise e

        return dframes

if __name__ == '__main__':
    reader = DataReader()
    for fname in tqdm(reader.all_bag_files):
        df = reader.bags_to_dataframe(fname)[0]
        # Compose new file name
        pkl_fname = '.'.join(os.path.basename(fname).split('.')[:-1]) + '.pkl'
        df.to_pickle('data/' + pkl_fname)

    # data = reader.bags_to_dataframe(reader.all_bag_files)
    # with open('data/dataframes.pkl', 'wb') as file:
    #     pickle.dump(data, file)
