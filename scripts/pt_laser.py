#!/usr/bin/env python

import math, copy, random
import numpy as np

import rospy, rospkg

from std_msgs.msg import Bool, Empty, String, Float32, Float32MultiArray
from geometry_msgs.msg import PoseStamped, Pose

import dynamic_reconfigure.server as dr

import tf2_ros
import tf2_geometry_msgs
import tf_conversions as tfc
import PyKDL as kdl

from bioloid.serial_bus import SerialPort, SerialBus
from bioloid.device_type_parser import DeviceTypeParser
from bioloid.device_type import DeviceTypes
from bioloid.device import Device
from bioloid.bus import BusError
from serial import SerialException

class PtLaser:
    def __init__(self):
        self.publish_rate = rospy.get_param('~publish_rate', 5)

        self.initialized = False
        self.compliance_slope = 32
        self.initialized = self.init_devices()

        self.laser_dot_position = rospy.Subscriber('/laser_dot/pose', Pose, self.follow)
        self.hz =rospy.get_param("~frequency")
        self.step= 10
        self.rate = rospy.Rate(self.hz)

        self.turret_height = 8

    def init_devices(self):
        baud_rate = rospy.get_param('~baud_rate', 1000000)
        port = rospy.get_param('~port', '/dev/ttyUSB0')

        pkg = rospkg.RosPack()
        dev_conf_path = pkg.get_path('hmri_pt_laser') + '/devcfg'

        self.joint_ids = rospy.get_param('~joint_ids', [1, 2])
        self.zeros = {}
        self.zeros[self.joint_ids[0]] = 150.0
        self.zeros[self.joint_ids[1]] = 60.0

        try:
            serial_port = SerialPort(port = port, baudrate = baud_rate)
        except SerialException:
            rospy.logfatal('Unable to open \'{}\''.format(port))
            return False

        self.bus = SerialBus(serial_port, show_packets = True)

        dev_types = DeviceTypes()
        parser = DeviceTypeParser(dev_types)
        parser.parse_dev_type_files(dev_conf_path)
        servo_type = dev_types.get('servo')
        if not servo_type:
            rospy.logfatal('Unable to find device type \'servo\'')
            return False

        self.dev = {}
        self.led = {}
        self.vel = {}
        self.pos = {}
        self.ccw_lim = {}
        self.cw_lim = {}
        self.cur_pos = {}
        self.cw_comp_slope = {}
        self.ccw_comp_slope = {}

        for i in self.joint_ids:
            self.dev[i] = Device(self.bus, i, servo_type)

            ping = {}
            while not self.dev[i].ping():
                rospy.logerr('Device {} is not responding. Will try again later'.format(i))
                rospy.sleep(0.5)

            self.led[i] = self.dev[i].get_dev_reg('led')
            self.vel[i] = self.dev[i].get_dev_reg('moving-speed')
            self.pos[i] = self.dev[i].get_dev_reg('goal-position')
            self.cur_pos[i] = self.dev[i].get_dev_reg('present-position')

            self.ccw_lim[i] = self.dev[i].get_dev_reg('ccw-angle-limit')
            self.cw_lim[i] = self.dev[i].get_dev_reg('cw-angle-limit')
            if i==1 and self.cw_lim[i].get() != 60.0:
                self.cw_lim[i].set(60.0)
                self.ccw_lim[i].set(240.0)
            if i==2 and self.cw_lim[i].get() != 30.0:
                self.cw_lim[i].set(30.0)
                self.ccw_lim[i].set(150.0)
            #rospy.loginfo('J{} pos: {} lim: ({}, {})'.format(i, self.pos[i].get() - self.zeros[i], self.cw_lim[i].get(), self.ccw_lim[i].get()))
            rospy.loginfo('J{} pos: {} lim: ({}, {})'.format(i, self.pos[i].get() - self.zeros[i], self.cw_lim[i].get() - self.zeros[i], self.ccw_lim[i].get() - self.zeros[i]))

            
            self.cw_comp_slope[i] = self.dev[i].get_dev_reg('cw-comp-slope')
            self.ccw_comp_slope[i] = self.dev[i].get_dev_reg('ccw-comp-slope')
            self.cw_comp_slope[i].set(self.compliance_slope)
            self.ccw_comp_slope[i].set(self.compliance_slope)
            self.bus.send_action()
            rospy.loginfo('J{} cw slope: {} ccw slope: {}'.format(i, self.cw_comp_slope[i].get(), self.ccw_comp_slope[i].get()))
        return True

    def fk(self, base_angle, upper_angle):
        h = self.turret_height
        dist_ground_end = h/math.cos(upper_angle)
        base_i = np.sqrt(dist_ground_end**2 - h**2)
        x = base_i*math.cos(base_angle)
        y = base_i*math.sin(base_angle)
        return x,y
    
    def ik(self, x, y):
        h = self.turret_height
        base_i = np.sqrt((x**2)+(y**2))
        base_angle = -90+np.degrees(np.arccos(x/base_i))
        dist_ground_end = np.sqrt(base_i**2+h**2)
        upper_angle = -90+np.degrees(np.arccos(h/dist_ground_end))
        rospy.loginfo('base_angle: {}'.format(base_angle))
        rospy.loginfo('upper_angle: {}'.format(upper_angle))
        return base_angle, upper_angle


    def run_trajectory(self, shape):
        if shape == 'square':
            A = [-20, 20]
            B = [-20, 60]
            C = [20, 60]
            D = [20, 20]
            points = []
            step = 1
            for i in range(A[1], B[1], step):
                points.append([A[0], i])
            for i in range(B[0], C[0], step):
                points.append([i, B[1]])
            for i in range(C[1], D[1], -step):
                points.append([C[0], i])
            for i in range(D[0], A[0], -step):
                points.append([i, D[1]])
            points.append(A)
            print points
            pose= Pose()
            for point in points:
                pose.position.x = point[0]
                pose.position.y = point[1]
                self.follow(pose)
                self.rate.sleep()
        elif shape== "8":
            t=-90
            pose= Pose()
            while t<1000:
                rad = math.radians(t)
                scale = 2 / (3 - math.cos(2*rad));
                x = 30*scale * math.cos(rad);
                y = 30+(30*scale * math.sin(2*rad) / 2);
                rospy.loginfo("(x, y): (%.5f, %.5f) " % (x, y))
                pose.position.x = x
                pose.position.y = y
                self.follow(pose)
                t+=self.step
                self.rate.sleep()
        elif shape== "bf":
            i=0
            pose= Pose()
            while i<1000:
                if i%2==0:
                    y = 100
                else:
                    y=20
                x=0
                pose.position.x = x
                pose.position.y = y
                self.follow(pose)
                i+=1
                self.rate.sleep()
                #print i
    

    def follow(self, j):
        if not self.initialized:
            return
        print j
        x = j.position.x#j[0]#
        y = j.position.y#j[1]#
        if (x != 0 or y !=0):
            alpha, beta = self.ik(x, y)
        else:
            alpha = 0
            beta=0
        print alpha
        print beta
        new_pos = {}
        new_pos_print = {}
        vel = {}
        
        # > 360
        max_dist = 0 # deg

        max_vel = 114.0 # RPM
        max_t = 0 # sec

        '''for idx, i in enumerate(self.joint_ids):
            rospy.loginfo('cur_pos[{}]: {}'.format(i, self.cur_pos[i].get() - self.zeros[i]))
        '''
        aux=[0,0]
        aux[0] = alpha
        aux[1]= beta
        for idx, i in enumerate(self.joint_ids):
            new_pos[i] = aux[idx] + self.zeros[i]
            new_pos_print[i] = aux[idx]
            '''dist = math.fabs(self.cur_pos[i].get() - new_pos[i])
            if dist > max_dist:
                max_dist = dist'''

        max_t = max_dist / (114.0)

        rospy.loginfo('max_t: {}'.format(max_t))

        '''for i in self.joint_ids:
            if max_t:
                vel[i] = math.fabs(self.cur_pos[i].get() - new_pos[i]) / max_t
            else:
                vel[i] = 0.0'''

        rospy.loginfo('new_pos: {}'.format(new_pos_print))
        rospy.loginfo('Vel: {}'.format(vel))

        for i in self.joint_ids:
            #self.led[i].set(0)
            is_error = False
            try:
                #self.vel[i].set(60.0)#self.vel[i].set(vel[i])
                self.pos[i].set(new_pos[i], deferred = True)

            except BusError as e:
                # Set LED
                self.led[i].set(1)
                # Don't move
                self.pos[i].set(self.cur_pos[i].get())
                rospy.logerr_throttle(0.5, e)
                is_error = True

        if not is_error:
            self.bus.send_action()
            return

    def run(self):
        while not rospy.is_shutdown():
            try:
                if rospy.get_param("~joystick"):
                    print self.ik(0,50)
                    pass
                else:
                    #self.pub_joint_pose.publish(self.cur_joint_pos())
                    #shape = raw_input("Insert the shape you want to draw:\n")
                    self.run_trajectory(rospy.get_param("~shape"))
                    #loop_rate.sleep()
                continue
            except rospy.ROSException, e:
                if e.message == 'ROS time moved backwards':
                    rospy.logwarn("Saw a negative time change, resetting.")

if __name__ == '__main__':
    rospy.init_node('pt_laser', anonymous = False)
    pt_laser = PtLaser()

    try:
        pass
        pt_laser.run()
    except rospy.ROSInterruptException:
        rospy.logdebug('Exiting')
        pass
