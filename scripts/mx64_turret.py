#!/usr/bin/env python

import math, copy, random
import numpy as np

import rospy, rospkg

from std_msgs.msg import Bool, Empty, String, Float32, Float32MultiArray
from geometry_msgs.msg import PoseStamped, Pose
from sensor_msgs.msg import JointState

import dynamic_reconfigure.server as dr

import tf2_ros
import tf2_geometry_msgs
import tf_conversions as tfc
import PyKDL as kdl

from dynamixel_workbench_msgs.srv import JointCommand

class dynamixelTurret:
    def __init__(self):
        self.publish_rate = rospy.get_param('~publish_rate', 5)

        self.initialized = False
        self.compliance_slope = 32
        self.compliance_margin = 0
        self.speed= 50

        self.joint_ids = rospy.get_param('~joint_ids', [1, 2])
        print self.joint_ids
        self.zeros = {}
        self.zeros[self.joint_ids[0]] = 0.0
        self.zeros[self.joint_ids[1]] = 0.0
        
        rospy.wait_for_service('/joint_command')
        self.set_joint_angle_srv = rospy.ServiceProxy('/joint_command', JointCommand)

        self.joint_state_sub = rospy.Subscriber('joint_state', JointState, self.set_joint_angle)

        self.hz =rospy.get_param("~frequency", 50)
        self.rate = rospy.Rate(self.hz)

    def set_joint_angle(self, msg):
        angles = msg.position
        new_pos = {}
        new_pos_print = {}
        vel = {}
        
        
        for idx, i in enumerate(self.joint_ids):
            new_pos[i] = angles[idx] + self.zeros[i]
            if i == 2:
                new_pos[i] = -angles[idx]
            self.set_joint_angle_srv("rad", i, math.radians(new_pos[i]))
            
            
if __name__ == '__main__':
    rospy.init_node('pt_laser', anonymous = False)
    pt_laser = dynamixelTurret()
    rospy.spin()
